# @ssb-graphql/group

## Types

### Group

- **`id`:** Cloaked group identifier.
- **`root`:** Group creation message.

#### TODO

- **`authors`:** Authors in group.
- **`key`:** Group key. Add this when we have "better GraphQL security".

## API

exact names TBD, but something like:

## Mutation: createGroup()

Create new group.

Returns group.


### Query: listGroups

Return array of groups that you are a member of.

### getGroup(id)

Return group identified by `id`.



## inviteToGroup(groupId, [author])

Add array of members to group.

Note that SSB-Tribes only allows 15 members to be added per message.
