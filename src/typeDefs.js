const gql = require('graphql-tag')

module.exports = gql`
  scalar CustomFieldInput

  input GroupApplicationAnswerInput {
    question: String
    answer: String
  }

  input PersonGroupCustomFieldInput {
    key: String!
    # NOTE: the type is only used on fields of type date
    type: String
    value: CustomFieldInput
  }

  type Group {
    id: ID!
    root: String!
    poBoxId: String
    subGroups: [Group]
  }

  type GroupApplication {
    id: ID!
    version: String!
    group: TribeFaces
    applicant: Person
    applicantId: String
    addMember: [String]
    decision: GroupApplicationDecision
    answers: [GroupApplicationAnswer]
    history: [GroupApplicationHistory]
  }

  type GroupApplicationDecision {
    accepted: Boolean
    addMember: String
  }

  type GroupApplicationAnswer {
    question: String
    answer: String
  }

  enum GroupApplicationHistoryType {
    answers
    comment
    decision
  }

  interface GroupApplicationHistory {
    type: GroupApplicationHistoryType
    author: Person
    authorId: String
    timestamp: String
  }

  type GroupApplicationCommentHistory implements GroupApplicationHistory {
    type: GroupApplicationHistoryType
    author: Person
    authorId: String
    timestamp: String
    comment: String
  }

  type GroupApplicationAnswerHistory implements GroupApplicationHistory {
    type: GroupApplicationHistoryType
    author: Person
    authorId: String
    timestamp: String
    answers: [GroupApplicationAnswer]
  }

  type GroupApplicationDecisionHistory implements GroupApplicationHistory {
    type: GroupApplicationHistoryType
    author: Person
    authorId: String
    timestamp: String
    decision: GroupApplicationDecision
  }

  extend type Query {
    getGroup(id: String!): Group
    listGroups: [Group]
    listSubGroups(id: String!): [Group]
    listGroupAuthors(id: ID!): [Person]
    getGroupApplication(id: String!): GroupApplication
    listGroupApplications(
      groupId: String
      accepted: Boolean
    ): [GroupApplication]
  }

  extend type Mutation {
    createGroup(nothing: Boolean): Group
    createSubGroup (id: String!): Group

    createGroupApplication(
      groupId: String!
      answers: [GroupApplicationAnswerInput]
      comment: String
      customFields: [PersonGroupCustomFieldInput]
    ): ID

    acceptGroupApplication(
      id: String!,
      comment: String,
      groupIntro: String
    ): ID

    declineGroupApplication(id: String!, reason: String): ID
  }
`
