const pull = require('pull-stream')
const paraMap = require('pull-paramap')

module.exports = function ListGroupAuthors (ssb, getProfile) {
  return function listGroupAuthors (id, cb) {
    ssb.tribes.listAuthors(id, (err, feedIds) => {
      if (err) return cb(err)

      pull(
        pull.values(feedIds),
        paraMap((feedId, cb) => {
          getProfile(feedId, (err, profile) => {
            if (err) {
              if (err.message.match(/unable to find public profile/)) {
                return cb(null, null)
              }
              return cb(err)
            }

            // CHECK - might need to add feedId back in?
            cb(null, profile)
          })
        }),
        pull.filter(Boolean), // filter out null records
        pull.collect((err, profiles) => {
          if (err) return cb(err)

          cb(null, profiles)
        })
      )
    })
  }
}
