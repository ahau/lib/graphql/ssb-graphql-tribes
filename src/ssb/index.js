const { promisify: p } = require('util')
const ListGroupAuthors = require('./query/list-group-authors')

module.exports = function methods (ssb, { getProfile }) {
  return promisify({
    listGroupAuthors: ListGroupAuthors(ssb, getProfile)

  })
}

function promisify (obj) {
  Object.entries(obj).forEach(([name, method]) => {
    obj[name] = p(method)
  })

  return obj
}
