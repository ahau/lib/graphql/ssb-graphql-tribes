const pull = require('pull-stream')
const paraMap = require('pull-paramap')
const get = require('lodash.get')
const { promisify: p } = require('util')
const CustomField = require('graphql-custom-field')
const methods = require('./ssb')

module.exports = (ssb, { getProfile }) => {
  const {
    listGroupAuthors
  } = methods(ssb, { getProfile })

  const getProfileById = p(getProfile)
  const getHistoryBody = ({ body }) => body
  const getApplicationAuthor = ({ profileId, author }) => getProfileById(profileId || author)
  const getApplicationAuthorId = ({ author: feedId }) => feedId

  const getTribe = async (id, cb) => {
    ssb.tribes.get(id, (err, tribe) => {
      if (err) cb(err)
      else cb(null, { id, ...tribe })
    })
  }

  const getSubGroupsByGroupId = (groupId, cb) => {
    ssb.tribes.findSubGroupLinks(groupId, (err, links) => {
      if (err) return cb(err)

      pull(
        pull.values(links),
        paraMap(
          (link, cb) => getTribe(link.subGroupId, cb),
          5
        ),
        pull.collect(cb)
      )
    })
  }

  return {
    CustomFieldInput: CustomField, // Scalar
    Query: {
      getGroup: (_, { id }) => p(getTribe)(id),
      listGroups: async () => {
        const groupIds = await p(ssb.tribes.list)()
        const results = await Promise.all(
          groupIds.map(id => p(getTribe)(id))
        )
        return results
      },
      listSubGroups: async (_, { id }) => p(getSubGroupsByGroupId)(id),
      listGroupAuthors: (_, { id }) => listGroupAuthors(id),
      getGroupApplication: (_, { id }) => p(ssb.registration.tribe.get)(id),
      listGroupApplications: async (_, { groupId = null, accepted = null }) => {
        const list = await p(ssb.registration.tribe.list)({
          get: true,
          groupId,
          accepted
        })

        return addProfileToApplicationHistories(list)
      }
    },
    Group: {
      subGroups: ({ groupId }) => p(getSubGroupsByGroupId)(groupId)
    },
    GroupApplication: {
      applicant: ({ profileId, applicantId }) => {
        return getProfileById(profileId || applicantId)
      },
      group: async ({ groupId }) => {
        const group = await p(ssb.profile.findByGroupId)(groupId)

        const pubProfile = []
        for await (const profile of group.public) {
          const res = await getProfileById(profile.key)
          pubProfile.push(res)
        }
        const privProfile = []
        for await (const profile of group.private) {
          const res = await getProfileById(profile.key)
          privProfile.push(res)
        }
        return {
          id: groupId,
          public: pubProfile,
          private: privProfile
        }
      }
    },
    GroupApplicationHistory: {
      __resolveType (history, context, info) {
        switch (history.type) {
          case 'answers': return 'GroupApplicationAnswerHistory'
          case 'comment': return 'GroupApplicationCommentHistory'
          case 'decision': return 'GroupApplicationDecisionHistory'
          default: return null
        }
      }
    },
    GroupApplicationCommentHistory: {
      comment: getHistoryBody,
      author: getApplicationAuthor,
      authorId: getApplicationAuthorId
    },
    GroupApplicationAnswerHistory: {
      answers: getHistoryBody,
      author: getApplicationAuthor,
      authorId: getApplicationAuthorId
    },
    GroupApplicationDecisionHistory: {
      decision: getHistoryBody,
      author: getApplicationAuthor,
      authorId: getApplicationAuthorId
    },
    GroupApplicationAnswer: {
      question: a => a.q,
      answer: a => a.a
    },
    Mutation: {
      // TODO: Why do I get an error when `createGroup()` doesn't have any arguments?
      // TODO: How does this handle errors?
      createGroup: async () => {
        const { groupId, groupInitMsg } = await p(ssb.tribes.create)(null)

        console.log('!!! createGroup is deprecated')

        return {
          id: groupId,
          root: groupInitMsg.key
        }
      },
      createSubGroup: async (_, { id }) => {
        const {
          groupId: subGroupId,
          groupInitMsg,
          poBoxId
        } = await p(ssb.tribes.subtribe.create)(id, { addPOBox: true })

        return {
          id: subGroupId,
          root: groupInitMsg.key,
          poBoxId
        }
      },
      createGroupApplication: async (_, { groupId, answers, comment, customFields = [] }, context) => {
        /* find group poBoxId */
        const profiles = await p(ssb.profile.findByGroupId)(groupId)
        const poBoxId = getPOBoxFromProfiles(profiles)
        if (!poBoxId) throw new Error(`unable to find poBoxId for group ${groupId}`)

        const recps = [poBoxId, ssb.id]

        /* publish a profile/person/admin for the kaitiaki of the group */
        const source = await p(ssb.profile.person.source.get)(context.personal.profileId)
        const details = {
          ...copyDetails(source),
          authors: { add: [ssb.id, '*'] }, // allow me + kaitiaki to edit
          recps
        }

        // here we need to load the community, and make sure we only save custom fields that are
        // defined
        const customFieldDefs = get(profiles, 'public[0].customFields', {})
        details.customFields = mapCustomFields(customFields.filter(field => customFieldDefs[field.key]))

        const adminProfileId = await p(ssb.profile.person.admin.create)(details)
        await p(ssb.profile.link.create)(adminProfileId) // links it to ourself

        if (answers) answers = answers.map(d => ({ q: d.question, a: d.answer }))

        // returns the id of the application
        return p(ssb.registration.tribe.create)(groupId, { answers, comment, profileId: adminProfileId, recps })
      },
      acceptGroupApplication: async (_, { id, comment, applicationComment, groupIntro }) => {
        comment = comment || applicationComment

        /* create profile/person/group version of their profile */
        const { applicantId, groupId, profileId } = await p(ssb.registration.tribe.get)(id)
        const adminProfile = await p(ssb.profile.person.admin.get)(profileId)

        const details = copyDetails(adminProfile)
        delete details.address
        delete details.email
        delete details.phone
        details.authors = { add: [applicantId] }
        details.recps = [groupId]

        // handle adding custom fields with visibleBy=members to the
        // members group profile
        const profiles = await p(ssb.profile.findByGroupId)(groupId)
        const customFieldDefs = get(profiles, 'public[0].customFields', {})

        const customFields = Object.entries(details.customFields)
          .filter(([key]) => {
            const fieldDef = customFieldDefs[key]
            if (!fieldDef) return false

            return fieldDef.visibleBy === 'members'
          })

        details.customFields = Object.fromEntries(customFields)

        const groupProfileId = await p(ssb.profile.person.group.create)(details)
        await p(ssb.profile.link.create)(groupProfileId, { feedId: applicantId })
        await p(ssb.profile.link.create)(groupProfileId, { profileId })
        // NOTE in future if a profile already exists for this person (and we have auithorship rights,
        // we could just hand that over to them and link them to it!

        return p(ssb.registration.tribe.accept)(id, { comment, groupIntro })
      },
      declineGroupApplication: async (_, { id, reason }) => {
        /* tombstone the admin profile */
        const { profileId } = await p(ssb.registration.tribe.get)(id)
        await p(ssb.profile.person.admin.tombstone)(profileId, { reason })

        return p(ssb.registration.tribe.reject)(id, { reason })
      }
    }
  }
}

function copyDetails (state) {
  const details = {}
  let value
  for (const field in state) {
    if (field === 'key') continue
    if (field === 'type') continue
    if (field === 'authors') continue
    if (field === 'originalAuthor') continue

    value = state[field]
    if (isEmpty(value)) continue

    if (field === 'altNames') details[field] = { add: value } // simple-set field
    else details[field] = value // overwrite fields
  }
  return details
}

function mapCustomFields (customFields) {
  const output = {}

  if (!customFields) return output

  customFields.forEach(field => {
    // NOTE: ssb-profile requires date custom fields to have a type of date
    // this is because there is no easy way to know if a field is a date or string
    if (field.type === 'date') {
      output[field.key] = {
        type: 'date',
        value: field.value
      }
    } else {
      output[field.key] = field.value
    }
  })

  return output
}

function isEmpty (value) {
  if (value === null) return true
  // NOTE should never be undefined
  if (typeof value === 'string' && value.length === 0) return true
  if (Array.isArray(value) && value.length === 0) return true
  // TODO: this line doesn't do anything
  if (value === {}) return true

  return false
}

function getPOBoxFromProfiles (profiles) {
  if (profiles.public.length === 0) return
  if (profiles.public.length > 1) console.warn('found more than one public community profile!')

  return profiles.public[0].poBoxId
}

function addProfileToApplicationHistories (list) {
  for (const application of list) {
    for (const historyLine of application.history) {
      if (historyLine.author === application.applicantId) {
        // decorate the history with the admin profileId they are applying with
        historyLine.profileId = application.profileId
      }
    }
  }

  return list
}
