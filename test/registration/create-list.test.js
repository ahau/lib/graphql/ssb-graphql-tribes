const test = require('tape')
const { promisify: p } = require('util')

const setup = require('./setup')
const { ListApplications, Replicate } = require('../lib/helpers')

// NOTE: this test includes a profileId when creating an application
test('registration create/list (v1)', async t => {
  const {
    kaitiaki,
    applicant,

    groupId,
    poBoxId,
    applicationId
  } = await setup()

  const replicate = Replicate(kaitiaki.ssb)

  /* check application details */
  const application = await p(applicant.ssb.registration.tribe.get)(applicationId)
  t.equal(application.groupId, groupId, 'application.group correct ')
  t.deepEqual(application.recps, [poBoxId, applicant.ssb.id], 'application > kaitiaki subgroup')
  const answers = [{ q: 'what is your favourate colour?', a: 'blue' }]
  t.deepEqual(application.answers, answers, 'application.answers')

  /* application.profile */
  const adminProfile = await p(applicant.ssb.profile.person.admin.get)(application.profileId)
  t.equal(adminProfile.preferredName, 'tama', 'kaitiaki profile has my preferredName')
  t.equal(adminProfile.phone, '12345', 'kaitiaki profile has my phone')

  await replicate(applicant.ssb, kaitiaki.ssb)

  /* kaitiaki loads applications */
  const kaitiakiListApplications = ListApplications(kaitiaki.apollo, t)
  const list = await kaitiakiListApplications(null) // null = not responded to?

  t.deepEquals(
    list,
    [
      {
        id: applicationId,
        group: { // TODO query for other details
          id: groupId
        },
        applicantId: applicant.ssb.id,
        applicant: {
          id: adminProfile.key, // or feedId?
          preferredName: 'tama'
        },
        decision: null,
        answers: [{
          question: 'what is your favourate colour?',
          answer: 'blue'
        }],
        history: [
          {
            type: 'answers',
            authorId: applicant.ssb.id, // applicant feed id
            author: { // TODO: should these still point to the applicants public profile?
              id: adminProfile.key,
              preferredName: 'tama',
              phone: '12345'
            },
            timestamp: list[0].history[0].timestamp, // HACK
            answers: [{
              question: 'what is your favourate colour?',
              answer: 'blue'
            }]
          },
          {
            type: 'comment',
            authorId: applicant.ssb.id,
            author: {
              id: adminProfile.key,
              preferredName: 'tama',
              phone: '12345'
            },
            timestamp: list[0].history[1].timestamp, // HACK
            comment: 'Kia ora, thanks for giving me the opportunity to join!'
          }
        ]
      }
    ],
    'The kaitiaki sees the application from the applicant'
  )

  // WIP here
  // - appove the application
  // - should
  //    - add-member
  //    - mint new group profile + link
  //    - publish application approval
  //
  // - ALSO test decline
  //    - tombstone admin profile
  //    - publish application decline

  applicant.ssb.close()
  kaitiaki.ssb.close()
  t.end()
})
