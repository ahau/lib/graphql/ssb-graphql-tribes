const test = require('tape')
const { promisify: p } = require('util')

const setup = require('./setup')
const { ListApplications, Replicate, AcceptGroupApplication } = require('../lib/helpers')

// NOTE: this test includes a profileId when creating an application
test('registration accept (v1)', async t => {
  const {
    kaitiaki,
    applicant,

    groupId,
    applicationId
  } = await setup()

  const replicate = Replicate(kaitiaki.ssb)
  const acceptGroupApplication = AcceptGroupApplication(kaitiaki.apollo)
  await replicate(applicant.ssb, kaitiaki.ssb)

  /* kaitiaki approves application */
  // - should
  //    - add-member
  //    - mint new group profile + link
  //    - publish application approval

  await acceptGroupApplication(applicationId, {
    comment: 'Kia ora Mix, you are very welcome!',
    groupIntro: 'Welcome Mix to the group!'
  })

  let list = await ListApplications(kaitiaki.apollo, t)(true)
  t.equal(list[0].id, applicationId, 'kaitiaki approves, then application marked as "accepted"')

  list = await ListApplications(kaitiaki.apollo, t)(null)
  t.equal(list.length, 0, 'no un-responded applications')

  await replicate(kaitiaki.ssb, applicant.ssb)

  list = await ListApplications(applicant.apollo, t)(true)
  t.equal(list[0].id, applicationId, 'applicant sees approved application')

  await new Promise(resolve => setTimeout(resolve, 500)) // sleep

  const profiles = await p(applicant.ssb.profile.findByFeedId)(applicant.ssb.id, { groupId, selfLinkOnly: false })
  const groupProfile = profiles.other.private[0]

  t.deepEqual(
    {
      originalAuthor: groupProfile.originalAuthor,
      recps: groupProfile.recps,

      preferredName: groupProfile.preferredName,
      phone: groupProfile.phone,
      authors: groupProfile.authors
    },
    {
      originalAuthor: kaitiaki.ssb.id,
      recps: [groupId],

      preferredName: 'tama',
      phone: undefined, // not on group profile!
      authors: {
        [applicant.ssb.id]: [{ start: 13, end: null }]
      }
    },
    'a group profile was made for me by the kaitiaki'
  )

  applicant.ssb.close()
  kaitiaki.ssb.close()
  t.end()
})
