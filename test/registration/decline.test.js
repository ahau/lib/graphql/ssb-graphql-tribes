const test = require('tape')
const { promisify: p } = require('util')

const setup = require('./setup')
const { ListApplications, Replicate } = require('../lib/helpers')

// NOTE: this test includes a profileId when creating an application
test('registration decline (v1)', async t => {
  const {
    kaitiaki,
    applicant,

    poBoxId,
    applicationId
  } = await setup()

  const replicate = Replicate(kaitiaki.ssb)
  await replicate(applicant.ssb, kaitiaki.ssb)

  /* kaitiaki declines application */
  // - should
  //    - publish application decline
  //    - tombstone the admin profile

  await kaitiaki.apollo.mutate({
    mutation: `
      mutation ($id: String!, $reason: String) {
        declineGroupApplication(id: $id, reason: $reason)
      }
    `,
    variables: {
      id: applicationId,
      reason: 'This is for my whanau only!'
    }
  })

  let list = await ListApplications(kaitiaki.apollo, t)(false)
  t.equal(list[0].id, applicationId, 'kaitiaki declines, then application marked as "accepted"')

  list = await ListApplications(kaitiaki.apollo, t)(null)
  t.equal(list.length, 0, 'no un-responded applications')

  await replicate(kaitiaki.ssb, applicant.ssb)

  list = await ListApplications(applicant.apollo, t)(false)
  t.equal(list[0].id, applicationId, 'applicant sees approved application')

  await new Promise(resolve => setTimeout(resolve, 500)) // sleep

  const profiles = await p(applicant.ssb.profile.findByFeedId)(applicant.ssb.id)
  t.equal(
    profiles.private.find(profile => profile.recps[0] === poBoxId),
    undefined,
    'admin profile has been tombstoned'
  )

  applicant.ssb.close()
  kaitiaki.ssb.close()
  t.end()
})
