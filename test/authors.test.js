const test = require('tape')
const { replicate } = require('scuttle-testbot')
const testBot = require('./test-bot')
const { promisify: p } = require('util')

test('listGroupAuthors', async t => {
  const { ssb: alice, apollo } = await testBot()
  const { ssb: bob } = await testBot()

  const aliceProfileId = await p(alice.profile.person.public.create)({
    preferredName: 'alice',
    authors: { add: ['*'] },
    allowPublic: true
  })
  await p(alice.profile.link.create)({
    type: 'feed-profile',
    profile: aliceProfileId,
    allowPublic: true
  })

  const bobProfileId = await p(bob.profile.person.public.create)({
    preferredName: 'bob',
    authors: { add: ['*'] },
    allowPublic: true
  })
  await p(bob.profile.link.create)({
    type: 'feed-profile',
    profile: bobProfileId,
    allowPublic: true
  })

  await replicate({
    from: bob,
    to: alice,
    name: id => (id === alice.id) ? 'alice' : 'bob'
  })

  const { groupId } = await p(alice.tribes.create)({})
  await p(alice.tribes.invite)(groupId, [bob.id], {})

  // // TODO (later?) the feedId isnt on a person profile any more,
  // // it is defined in ssb-ahau/graphql-authors/typeDefs
  // // and it is called originalAuthor
  // // the problem is this module doesnt have access to that
  // // commented out the feedId tests for now

  const result = await apollo.mutate({
    mutation: `query($groupId: ID!) {
      listGroupAuthors(id: $groupId) {
        id
        # feedId: originalAuthor
        preferredName
      }
    }`,
    variables: {
      groupId
    }
  })
  t.error(result.errors, 'get group authors without error')

  t.deepEqual(
    result.data.listGroupAuthors,
    [
      {
        id: aliceProfileId,
        // feedId: alice.id,
        preferredName: 'alice'
      },
      {
        id: bobProfileId,
        // feedId: bob.id,
        preferredName: 'bob'
      }
    ],
    'gets group authors'
  )

  alice.close()
  bob.close()
  t.end()
})
