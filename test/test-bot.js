
/* <<<< copy */
import AhauClient from 'ahau-graphql-client'
const ahauServer = require('ahau-graphql-server')
const fetch = require('node-fetch')

const port = new Set([])
port.generate = function () {
  let port = 31000 + Math.random() * 10000 | 0
  while (this.has(port)) {
    port = 31000 + Math.random() * 10000 | 0
  }
  this.add(port)
  return port
}
async function graphqlTesting (ssb, schemas, context = {}) {
  const graphqlPort = port.generate()
  const httpServer = await ahauServer({
    context,
    schemas,
    port: graphqlPort
  })
  ssb.close.hook((close, [cb]) => {
    httpServer.close()
    close(cb)
  })

  return new AhauClient(graphqlPort, { fetch, isTesting: true })
}
/* copy >>>> */

const Server = require('scuttle-testbot')

/* Monkey patch */
console.niceLog = (...args) => {
  args.forEach(arg => {
    if (typeof arg === 'string') console.log(arg)
    else console.log(JSON.stringify(arg, null, 2))
  })
}

module.exports = async function (opts = {}) {
  if (!opts.serveBlobs) opts.serveBlobs = {}
  if (!opts.serveBlobs.port) {
    opts.serveBlobs.port = 10000 + Math.floor(Math.random() * 3000)
  }

  const stack = Server
    .use(require('ssb-db2/core'))
    .use(require('ssb-classic'))
    .use(require('ssb-db2/compat/db'))
    .use(require('ssb-db2/compat/feedstate'))
    .use(require('ssb-db2/compat/ebt'))
    .use(require('ssb-db2/compat/history-stream'))
    .use(require('ssb-db2/compat/log-stream'))
    .use(require('ssb-db2/compat/post'))

    .use(require('ssb-box2'))
    .use(require('ssb-tribes'))
    .use(require('ssb-tribes-registration'))
    .use(require('ssb-blobs'))
    // .use(require('ssb-serve-blobs'))
    .use(require('ssb-profile'))
    .use(require('ssb-whakapapa'))
    .use(require('ssb-settings'))
    .use(require('ssb-recps-guard'))

  const ssb = stack({
    ...opts,
    noDefaultUse: true,
    box2: {
      ...opts.box2,
      legacyMode: true
    }
  })

  // ssb.on('log', m => console.log(JSON.stringify(m, null, 2)))
  // ssb.emit('log', { problem: true }) << example usage

  const main = require('@ssb-graphql/main')(ssb)
  const profile = require('@ssb-graphql/profile')(ssb)

  const whakapapa = require('@ssb-graphql/whakapapa')(ssb, { ...profile.gettersWithCache })
  const tribes = require('../')(ssb, { ...profile.gettersWithCache })

  let context = {}
  if (opts.loadContext) context = await main.loadContext()

  if (opts.debug) {
    ssb.post(m => {
      ssb.get({ id: m.key, private: true }, (_, value) => {
        console.log(value.author, value.sequence)
        console.log(JSON.stringify(value.content, null, 2))
        console.log()
      })
    })
  }

  const apollo = await graphqlTesting(ssb, [main, profile, whakapapa, tribes], context)

  return {
    ssb,
    apollo
  }
}
