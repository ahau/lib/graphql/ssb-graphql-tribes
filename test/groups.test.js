const test = require('tape')
const testBot = require('./test-bot')

test('createGroup, getGroup, listGroups', async t => {
  // Fundamentals
  const { ssb, apollo } = await testBot()

  // TODO: How do I run a mutation without inputs? This is the equivalent of
  // an empty HTTP POST.
  const createGroupResult = await apollo.mutate({
    mutation: `
      mutation($nothing: Boolean) {
        createGroup(nothing: $nothing) {
          id
          root
        }
      }`
  })
  t.error(createGroupResult.errors, 'createGroup')

  const group = createGroupResult.data.createGroup

  const getGroupResult = await apollo.query({
    query: `
      query($id: String!) {
        getGroup(id: $id) {
          root
        }
      }`,
    variables: {
      id: group.id
    }
  })
  t.error(getGroupResult.errors, 'getGroup query')
  t.equal(
    getGroupResult.data.getGroup.root,
    group.root
  )

  ///
  //
  const listGroupsResult = await apollo.query({
    query: `
      query {
        listGroups {
          id
        }
      }`
  })
  t.error(listGroupsResult.errors, 'listGroups query')
  t.deepEqual(listGroupsResult.data.listGroups, [{ id: group.id }])

  ssb.close(t.end)
})
